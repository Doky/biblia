### BIBLIA

Biblia est un bot discord permettant de gérer le contenu d'une bibliothèque dont les livres seront stocké sur discord. 
Le bot offre une interface interactive pour ajouter un livre et remplire une fiche de lecture. Il offre également un outil de recherche parmi la bibliothèque.

### License

This program is Free Software: You can use, study share and improve it at your will. Specifically you can redistribute and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
