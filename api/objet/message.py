from api.texte import texte





class Message:

    def __init__(self):
        """ + - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - + home + - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +"""

        self.home = f"« *Que voulez-vous faire ?* »" \
                                   f"\n\n\t📘 - **Ajouter** un ouvrage" \
                                   f"\n\t📝 - **Modifier** un ouvrage" \
                                   f"\n\t🔍 - **Rechercher** un ouvrage" \




    def search(self,session,emoji = False, langage = "fr"):

        """
        retourne le message de recherche part rapport a la session

        :param session: instance de la session
        :param emoji: bool, si oui renvois les emoji a afficher
        :param langage: la langue du massage
        :return: message ou liste d'emoji
        """

        if session.step == 1:
            if emoji:
                return ["🔑","📂","📅","🎬","📖","❤️","🔎","⭕"]
            else:

                self.reponse =  "\t" + texte["search"]["step1/1"][langage] + "\n\n** filtre de recherche** (optionnel)"
                self.fin = ""

                if session.fiche["mot_clef"] == "":
                    self.reponse = self.reponse + "\n" + texte["search"]["step1/4"][langage]
                else:
                    self.fin = "\n" + texte["search"]["step1/5"][langage] + session.fiche['mot_clef']

                if session.fiche["categories"] == []:
                    self.reponse = self.reponse + "\n" + texte["search"]["step1/6"][langage]
                else:
                    self.fin = self.fin + "\n" + texte["search"]["step1/7"][langage] + str(session.fiche["categories"])

                if session.fiche["siecle"] == "":
                    self.reponse = self.reponse + "\n" + texte["search"]["step1/8"][langage]
                else:
                    self.fin = self.fin + "\n" + texte["search"]["step1/9"][langage] + str(session.fiche["siecle"])

                if session.fiche["tkw"] == "":
                    self.reponse = self.reponse + "\n" + texte["search"]["step1/10"][langage]
                elif session.fiche["tkw"] == True:
                    self.fin = self.fin + "\n" + texte["search"]["step1/12"][langage]
                elif session.fiche["tkw"] == False:
                    self.fin = self.fin + "\n" + texte["search"]["step1/11"][langage]
                else:
                    self.fin = self.fin + "\n erreur"

                if session.fiche['readlist'] == "":
                    self.reponse = self.reponse + "\n" + texte["search"]["step1/13"][langage]
                elif session.fiche['readlist'] == True:
                    self.fin = self.fin + "\n" + texte["search"]["step1/14"][langage]
                elif session.fiche['readlist'] == False:
                    self.fin = self.fin + "\n" + texte["search"]["step1/15"][langage]
                else:
                    self.fin = self.fin + "\n erreur"

                if session.fiche['coupdecoeur'] == "":
                    self.reponse = self.reponse + "\n" + texte["search"]["step1/16"][langage]
                elif session.fiche['coupdecoeur'] == True:
                    self.fin = self.fin + "\n" + texte["search"]["step1/17"][langage]
                elif session.fiche['coupdecoeur'] == False:
                    self.fin = self.fin + "\n" + texte["search"]["step1/18"][langage]
                else:
                    self.fin = self.fin + "\n erreur"

                return self.reponse + "\n" + self.fin + "\n\n\n** command**\n" + texte["search"]["step1/2"][langage] +"\n" + texte["search"]["step1/3"][langage]



        elif session.step == 2:
            if emoji:
                return ["❌"]
            return texte["search"]["step2/1"][langage]

        elif session.step == 3:
            if emoji:
                return ["✅","❌"]
            return texte["search"]["step3/1"][langage]

        elif session.step == 4:
            if emoji:
                return ["❌"]
            return texte["search"]["step4/1"][langage]

        elif session.step == 5:
            if emoji:
                return ["✅","⏹","❌"]
            return texte["search"]["step5/1"][langage]

        elif session.step == 6:
            if emoji:
                return ["✅","⏹","❌"]
            return texte["search"]["step6/1"][langage]

        elif session.step == 7:
            if emoji:
                return ["✅","⏹","❌"]
            return texte["search"]["step7/1"][langage]







    def registration(self, session, emoji=False,valide=True, langage = "fr"):


        """
        renvoie le message de d'enregistrement part rapport a la session

        :param session: instance de la session
        :param emoji: si true, renvoie la liste d'emoji a ajouter en reaction au massage
        :param valide: bool si le poste est complet et pres a etre publier
        :param langage: langage du message
        :return: message ou liste d'emoji
        """


        if session.step == 0:
            if emoji:
                return ["⭕"]

            else:
                return texte["registration"]["step0/1"][langage]

        elif session.step == 1:  # message de base
            if emoji:
                if session.type == "edit": # emoji mode edit
                    if valide:
                        if session.admin:
                            return ["👤", "📜", "📅", "📝", "🎬", "🖼️", "✅", "⭕"]
                        return ["👤", "📜", "📅", "📝", "🎬", "🖼️", "✅", "⭕"]
                    return ["👤", "📜", "📅", "📝", "🎬", "🖼️", "⭕"]
                #emoji mode registration
                if valide:
                    if session.admin:
                        return ["👤", "📜", "📅", "📝", "📂", "🎬", "🖼️", "📖","✅", "⭕"]
                    return ["👤", "📜", "📅", "📝", "📂", "🎬", "🖼️", "📖","✅", "⭕"]
                return ["👤", "📜", "📅", "📝", "📂", "🎬", "🖼️", "📖", "⭕"]

            else: #message step 1
                registration = {
                    0 : "\n***Quelle information voulez vous modifier à la fiche du livre ?***",
                    1 : "\n***Quelle information voulez vous ajouter à la fiche du livre ?***",
                    2 : "\n\n***Information deja rentréé, vous pouvez les modifier***",
                    3 : "\n\t👤 - Auteur(s)",
                    4 : "\n\t📅 - Année de publication",
                    5 : "\n\t📝 - Citation ou Résumé",
                    6 : "\n\t🎬 - Cité en interview (optionnel)",
                    7 : "\n\t🖼️ - Image de couverture",
                    8 : "\n\t✅ - Valider la publication",
                    9 : "\n\t⭕ - Annuler la publication",
                    10: "\n\t❌ - Revennir au menus",
                    11: "\n\t🗑 - supprimer le livre",
                    12: "\n👇 Cliquez sur les réactions ci-dessous afin de renseigner leurs champs 👇",
                    13: "\n***imformation a completer pour faire la modification***",
                    14: "\n\t📂 - Catégorie",
                    15: "\n\t📖 - Fichier du livre",
                    16: "\n\t📜 - Titre"
                }

                self.message = ""
                self.fin = ""

                if session.type == "edit":
                    self.reponse = texte["registration"]["step1/1"][langage]
                else:
                    self.reponse = texte["registration"]["step1/2"][langage]




                if session.type != "edit":
                    if session.book['authors'] == "":
                        self.reponse = self.reponse + texte["registration"]["step1/4"][langage]
                    else:
                        self.fin = texte["registration"]["step1/4"][langage]

                    if session.book['title'] == "":
                        self.reponse = self.reponse + texte["registration"]["step1/17"][langage]
                    else:
                        self.fin = self.fin + texte["registration"]["step1/17"][langage]

                    if session.book['date'] == "":
                        self.reponse = self.reponse + texte["registration"]["step1/5"][langage]
                    else:
                        self.fin = self.fin + texte["registration"]["step1/5"][langage]

                    if session.book['description'] == "":
                        self.reponse = self.reponse + texte["registration"]["step1/6"][langage]
                    else:
                        self.fin = self.fin + texte["registration"]["step1/6"][langage]

                    if session.book['categories'] == "":
                        self.reponse = self.reponse + texte["registration"]["step1/15"][langage]
                    else:
                        self.fin = self.fin + texte["registration"]["step1/15"][langage]

                    if session.book['recommended_by'] == "":
                        self.reponse = self.reponse + texte["registration"]["step1/7"][langage]
                    else:
                        self.fin = self.fin + texte["registration"]["step1/7"][langage]

                    if session.book['img_cover'] == "":
                        self.reponse = self.reponse + texte["registration"]["step1/8"][langage]
                    else:
                        self.fin = self.fin + texte["registration"]["step1/8"][langage]

                    if session.book['file'] == "":
                        self.reponse = self.reponse + texte["registration"]["step1/16"][langage]
                    else:
                        self.fin = self.fin + texte["registration"]["step1/16"][langage]

                    if valide:
                        self.reponse = self.reponse + texte["registration"]["step1/9"][langage]

                    self.reponse = self.reponse + texte["registration"]["step1/10"][langage]

                    if self.fin != "":
                        self.fin = texte["registration"]["step1/3"][langage] + self.fin
                        self.reponse = self.reponse + "\n" + self.fin

                    return self.reponse


                else: # modification
                    if session.book['authors'] == {}:
                        self.fin = texte["registration"]["step1/4"][langage]
                    else:
                        self.reponse = self.reponse + texte["registration"]["step1/4"][langage]

                    if session.book['title'] == {}:
                        self.fin = texte["registration"]["step1/17"][langage]
                    else:
                        self.reponse = self.reponse + texte["registration"]["step1/17"][langage]

                    if session.book['date'] == "":
                        self.fin = self.fin + texte["registration"]["step1/5"][langage]
                    else:
                        self.reponse = self.reponse + texte["registration"]["step1/5"][langage]

                    if session.book['description'] == "":
                        self.fin = self.fin + texte["registration"]["step1/6"][langage]
                    else:
                        self.reponse = self.reponse + texte["registration"]["step1/6"][langage]

                    if session.book['recommended_by'] == "":
                        self.fin = self.fin + texte["registration"]["step1/7"][langage]
                    else:
                        self.reponse = self.reponse + texte["registration"]["step1/7"][langage]

                    if session.book['img_cover'] == "":
                        self.fin = self.fin + texte["registration"]["step1/8"][langage]
                    else:
                        self.reponse = self.reponse + texte["registration"]["step1/8"][langage]

                    if valide:
                        self.reponse = self.reponse + texte["registration"]["step1/9"][langage]

                    self.reponse = self.reponse + texte["registration"]["step1/10"][langage]

                    if session.admin:
                        self.reponse = self.reponse + texte["registration"]["step1/12"][langage]

                    if self.fin != "":
                        self.reponse = self.reponse + "\n" + texte["registration"]["step1/14"][langage] + self.fin

                    return self.reponse


        elif session.step == 2:  # titres
            if emoji:
                return ["❌"]
            elif session.type == "edit":
                return texte["registration"]["step2/1"][langage]
            return texte["registration"]["step2/2"][langage]

        elif session.step == 3:  # autheur
            if emoji:
                return ["❌"]
            elif session.type == "edit":
                return texte["registration"]["step3/1"][langage]
            return texte["registration"]["step3/2"][langage]

        elif session.step == 4:  # date
            if emoji:
                return ["❌"]
            elif session.type == "edit":
                return texte["registration"]["step4/1"][langage]
            return texte["registration"]["step4/2"][langage]

        elif session.step == 5:  # categorie
            if emoji:
                return ["❌"]
            elif session.type == "edit":
                return texte["registration"]["step5/1"][langage]
            return texte["registration"]["step5/2"][langage]

        elif session.step == 6:  # description
            if emoji:
                return ["❌"]
            elif session.type == "edit":
                return texte["registration"]["step6/1"][langage]
            return texte["registration"]["step6/2"][langage]

        elif session.step == 7:  # image cover
            if emoji:
                return ["❌"]
            elif session.type == "edit":
                return texte["registration"]["step7/1"][langage]
            return texte["registration"]["step7/2"][langage]

        elif session.step == 8:  # recommaned by
            if emoji:
                return ["❌"]
            elif session.type == "edit":
                return texte["registration"]["step8/1"][langage]
            return texte["registration"]["step8/2"][langage]

        elif session.step == 9:  # epup
            if emoji:
                return ["❌"]
            elif session.type == "edit":
                return texte["registration"]["step9/1"][langage]
            return texte["registration"]["step9/2"][langage]

        elif session.step == 10:  # publication
            if session.type == "edit":
                return texte["registration"]["step10/1"][langage]
            return texte["registration"]["step10/2"][langage]


    def erreur(self,mode,name):

        if mode == "edit":
            if name == "validation":
                return "toutes les donner ne sont pas remplis"
            return "erreur"


        elif mode == "registration":
            if name == "validation":
                return "toutes les donner ne sont pas remplis"
            return "erreur"


        elif mode == "search":
            return "erreur"


        else:
            return "une erreur c'est produite, contacter un dev' pour en savoir plus"

    def welcome(self, nom,langage = "fr"):
        return texte["welcome"][1][langage] + nom + texte["welcome"][2][langage]

    def help(self,langage = "fr"):
        return [texte["help"]["sommaire"][langage],
                texte["help"]["agencement"][langage],
                texte["help"]["fiche"][langage],
                texte["help"]["ajout"][langage],
                texte["help"]["modification"][langage],
                texte["help"]["recherche"][langage],
                texte["help"]["readlist"][langage],
                texte["help"]["coupdecoeur"][langage]]


bot_message = Message()
