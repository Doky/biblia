import os
import sqlite3
import json


#chemin
chemin_bibliotheque = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "bibliotheque.db")

chemin_provisoir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../data","bibliotheque.db")
chemin_compteur = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data","compteur.json")
chemin_compteur_provisoir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../data","compteur.json")

print(chemin_provisoir)

def creat_database():

    """
    créé les table si elle n'existe pas

    :return:
    """

    connection = sqlite3.connect(chemin_provisoir)
    curseur = connection.cursor()


    # table des membres
    curseur.execute("""
    CREATE TABLE IF NOT EXISTS membres(
        id INTEGER,
        nom TEXT,
        langage TEXT
    )
    """)

    # table des livres
    curseur.execute("""
        CREATE TABLE IF NOT EXISTS livre(
            id_fiche INTEGER,
            id_miroir INTEGER,
            id_channel INTEGER,
            title TEXT,
            authors TEXT,
            categories TEXT,
            description TEXT,
            date INTEGER,
            rocommended_by TEXT,
            url TEXT,
            saga TEXT
        )
        """)

    # table des lecture
    curseur.execute("""
            CREATE TABLE IF NOT EXISTS lecture(
            id_user INTEGER,
            id_fiche INTEGER
            )
        """)

    # table des like
    curseur.execute("""
                CREATE TABLE IF NOT EXISTS like(
                id_user INTEGER,
                id_fiche INTEGER
                )
            """)

    connection.commit()
    connection.close()



"""  ----------------------------------------------------------------------  bibliotheque  ---------------------------------------------------------------------"""

def ajouter_un_livres(book):

    """
    permet d'ajouter un libre a la base de donner

    :param book: dictionnaire des info du livre
    :return:
    """


    connection = sqlite3.connect(chemin_provisoir)




    curseur = connection.cursor()
    liste = [book["fiche_id"],book["miroir_id"],book["channel_id"], book["title"],book["authors"],book["categories"],book["description"],book["date"],book["recommended_by"],book["url"], ""]




    curseur.execute("""INSERT INTO livre VALUES(
                    :title,
                    :authors,
                    :date,
                    :categories,
                    :id_channel,
                    :description,
                    :rocommended_by,
                    :url,
                    :id_fiche,
                    :id_miroir,
                    :saga
                    )""",liste)


    connection.commit()


    connection.close()
    compteur_add_enregistre()
    print("le livre en enregistrer dans sqlite3")


def recuperer_bibliotheque():

    """
    permet de recuperer tous les livre dans la bilibotheque

    :return: liste de dictionnaire des livre de la bibliotheque
    """
    connection = sqlite3.connect(chemin_provisoir)
    curseur = connection.cursor()
    try:
        curseur.execute("SELECT * FROM livre")
        data = curseur.fetchall()
        print(data)

        connection.commit()
        return data
    except:
        return False
    finally:
        connection.close()


def recuperer_un_livre(id,controll=False):

    """
    recupere un livre grace a sont id

    :param id: id du livre (int)
    :param controll: si true, renvoie juste bool true/false pour verifier que le livre existe
    :return: fiche du livre
    """
    connection = sqlite3.connect(chemin_provisoir)
    curseur = connection.cursor()
    curseur.execute("SELECT * FROM livre WHERE id_fiche=?",(id,))
    data = curseur.fetchone()

    connection.commit()
    connection.close()
    if data == None or data == False or data == []:
        return False
    if controll:
        return True
    return _mise_forme_data(data)


def suprimer_un_livre(id):

    """
    suprime un livre via sont id

    :param id: id du livre
    :return: bool
    """

    connection = sqlite3.connect(chemin_provisoir)
    curseur = connection.cursor()
    try:
        curseur.execute("DELETE FROM livre WHERE id_livre=?",(id,))
        connection.commit()
        connection.close()
        compteur_remove_enregistre()
        return True
    except:
        return False
    finally:
        connection.close()


    pass

def modifier_un_livre(id,book):

    """
    modifie la fiche d'un livre dans la database

    :param id: id du livre
    :param book: dictionnaire de la fiche du livre
    :return:
    """

    if recuperer_un_livre(id) == None:
        return False

    connection = sqlite3.connect(chemin_provisoir)
    curseur = connection.cursor()

    liste = [book["fiche_id"], book["miroir_id"], book["channel_id"], book["title"], book["authors"],
             book["categories"], book["description"], book["date"], book["recommended_by"], book["url"], "",book["fiche_id"]]
    curseur.execute("""
    UPDATE livre SET 
                id_fiche = :1,
                id_miroir = :2,
                id_channel = :3,
                title = :4,
                authors = :5,
                categories = :6,
                description = :7,
                date = :8,
                rocommended_by = :9,
                url = :10,
                saga = :11
    WHERE id_fiche = :fiche_id
    """,liste)
    connection.commit()

    connection.close()




def _mise_forme_data(fiche):

    """
    passe sous forme de dictionnaire la fiche d'un livre sortie de la database

    :param fiche: liste de la fiche du livre
    :return: dict fiche du livre
    """


    book = {
        "fiche_id": fiche[0],
        "miroir_id": fiche[1],
        "channel_id": fiche[2],
        "title": fiche[3],
        "authors": fiche[4],
        "categories": fiche[5],
        "description": fiche[6],
        "date": fiche[7],
        "recommended_by": fiche[8],
        "url": fiche[9],
        "img_cover": "",
        "file": ""}

    return book


async def recherche_dans_la_bibliotheque(fiche):

    """
    permet de faire une recherche dans la bibliotheque via des critere de selection

    :param fiche: dict des critere
    :return: liste resultat de la recherche
    """

    bibliotheque = recuperer_bibliotheque()

    list = []

    for livre in bibliotheque:
        livre = _mise_forme_data(livre)

        if fiche["mot_clef"]:

            if fiche["mot_clef"] in livre['authors'] \
                    or fiche["mot_clef"] in livre['title'] \
                    or fiche["mot_clef"] in livre['description']:
                pass

            else:
                livre = []

        if fiche["categories"] and livre != []:
            if livre['categories'][0] in fiche['categories']:
                pass
            else:
                livre = []

        if fiche["siecle"] and livre != []:
            balisehaute = fiche["siecle"] * 100
            balisebasse = balisehaute - 100

            if livre['date'] < balisehaute and livre["date"] > balisebasse:
                pass

            else:
                livre = []

        if fiche['tkw'] and livre != []:
            if fiche['tkw'] == True:
                if livre['recommended_by']:
                    pass
                else:
                    livre = []

            elif fiche['tkw'] == False:
                if livre['recommended_by']:
                    livre = []
                else:
                    pass

        if fiche["readlist"] != "" and livre != []:
            if fiche["readlist"] == True:
                if lecture_exist(id_livre=livre["fiche_id"],id_user=fiche["user_id"]):
                    print("readlist: True, et le livre est garder")
                    pass

                else:
                    print("readlist: True, et le livre est suprimer")
                    livre = []


            elif fiche["readlist"] == False:
                if lecture_exist(id_livre=livre["fiche_id"],id_user=fiche["user_id"]):
                    print("readlist: False, et le livre est supprimer")
                    livre = []

                else:
                    print("readlist: False, et le livre est garder")
                    pass

        if fiche["coupdecoeur"] != "" and livre != []:
            if fiche["coupdecoeur"] == True:
                if like_exist(id_livre=livre["fiche_id"],id_user=fiche["user_id"]):
                    pass

                else:
                    livre = []


            elif fiche["coupdecoeur"] == False:
                if like_exist(id_livre=livre["fiche_id"],id_user=fiche["user_id"]):
                    livre = []

                else:
                    pass

        if livre != []:
            list.append(livre)

    compteur_add_recherche()
    return list





""" ----------------------------------------------------------------------    readlist    ----------------------------------------------------------------------"""



def ajouter_une_lecture(id_livre,id_user):

    """
    ajoute une lecture a la database
    la lecture est la trace qui dit qu'un utilisateur a lu un livre

    :param id_livre: id du livre
    :param id_user: id de l'utilisateur
    :return:
    """

    if not recuperer_un_livre(id_livre,controll=True) or lecture_exist(id_livre,id_user):

        return

    connection = sqlite3.connect(chemin_provisoir)
    curseur = connection.cursor()
    curseur.execute("""INSERT INTO lecture VALUES(
                    :1,
                    :2
                    )
    """,[id_user,id_livre])

    connection.commit()
    connection.close()
    compteur_add_lecture()


def suprimer_une_leture(id_livre,id_user):

    """
    supprime une lecture de la database

    :param id_livre: id du livre
    :param id_user: id de l'utilisateur
    :return:
    """

    if not recuperer_un_livre(id_livre,controll=True) or not lecture_exist(id_livre,id_user):
        return

    connection = sqlite3.connect(chemin_provisoir)
    curseur = connection.cursor()
    curseur.execute("""DELETE FROM lecture WHERE
                    id_user = :1 and
                    id_fiche = :2
                    """,(id_user, id_livre))
    connection.commit()
    connection.close()
    compteur_remove_lecture()

def lecture_exist(id_livre,id_user):

    """
    controlle qu'une lecture existe dans la database

    :param id_livre: id du livre
    :param id_user: id de l'utilisateur
    :return: bool
    """

    connection = sqlite3.connect(chemin_provisoir)
    curseur = connection.cursor()

    curseur.execute("SELECT * FROM lecture WHERE id_fiche=? and id_user=?",(id_livre,id_user))
    data = curseur.fetchall()

    connection.commit()
    connection.close()
    if data == [] or data == None:
        return False
    return True


def ajouter_un_like(id_livre,id_user):

    """
    ajoute un like a la database
    un like est l'info qu'un utilisateur a aimer un livre

    :param id_livre: id du livre
    :param id_user: id de l'utilisateur
    :return:
    """
    if not recuperer_un_livre(id_livre,controll=True or like_exist(id_livre,id_user)):
        return

    connection = sqlite3.connect(chemin_provisoir)
    curseur = connection.cursor()
    curseur.execute("""INSERT INTO like VALUES(
                    :1,
                    :2
                    )
    """, (id_user, id_livre))
    connection.commit()
    connection.close()
    compteur_add_coupdecoeur()

def supprimer_un_like(id_livre,id_user):

    """
    supprime un like de la database

    :param id_livre: id du livre
    :param id_user: id de l'utilisateur
    :return:
    """

    if not recuperer_un_livre(id_livre,controll=True) or like_exist(id_livre,id_user) == None:
        return

    connection = sqlite3.connect(chemin_provisoir)
    curseur = connection.cursor()
    curseur.execute("""DELETE FROM like WHERE(
                    id_user = :1 and
                    id_fiche = :2
                    )
    """, [id_user, id_livre])
    connection.commit()
    connection.close()
    compteur_remove_coupdecoeur()


def like_exist(id_livre,id_user):

    """
    controlle qu'un like existe dans la database

    :param id_livre: id du livre
    :param id_user: id de l'utilisateur
    :return: bool
    """

    connection = sqlite3.connect(chemin_provisoir)
    curseur = connection.cursor()

    curseur.execute("SELECT  * FROM like WHERE id_user=:a and id_fiche=:b ", (id_user, id_livre))
    data = curseur.fetchall()

    connection.commit()
    connection.close()
    if data == None or data == []:
        return False
    return True



""" ----------------------------------------------------------------------- compteur -----------------------------------------------------------------------"""


def biblia_compteur():

    """
    renvoie le compteur du bot

    :return: dict compteur
    """

    try:
        with open(chemin_compteur_provisoir, "r") as f:
            return json.load(f)
    except:
        return {"recherche": 0,
                "enregistre": 0,
                "lecture": 0,
                "like": 0}


def write_compteur(dict):
    with open(chemin_compteur_provisoir, "w") as f:
        json.dump(dict, f)


def compteur_add_recherche():
    compteur = biblia_compteur()
    compteur['recherche'] += 1
    write_compteur(compteur)


def compteur_add_enregistre():
    compteur = biblia_compteur()
    compteur['enregistre'] += 1
    write_compteur(compteur)


def compteur_remove_enregistre():
    compteur = biblia_compteur()
    compteur['enregistre'] -= 1
    write_compteur(compteur)


def compteur_add_lecture():
    compteur = biblia_compteur()
    compteur['lecture'] += 1
    write_compteur(compteur)


def compteur_remove_lecture():
    compteur = biblia_compteur()
    compteur['lecture'] -= 1
    write_compteur(compteur)


def compteur_add_coupdecoeur():
    compteur = biblia_compteur()
    compteur['like'] += 1
    write_compteur(compteur)


def compteur_remove_coupdecoeur():
    compteur = biblia_compteur()
    compteur['like'] -= 1
    write_compteur(compteur)

